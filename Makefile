## Le Figaro (devops-nm@lefigaro.fr)

.DEFAULT_GOAL := help

.PHONY: help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?## .*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[36m##/[33m/'

# Variables
DOCKER_COMPOSE      = docker-compose
PHP                 = $(DOCKER_COMPOSE) run --rm php-tools
NODE                = $(DOCKER_COMPOSE) run --rm node-tools
DEPLOY              = $(DOCKER_COMPOSE) run --rm deploy-tools
SYMFONY             = $(PHP) bin/console
COMPOSER            = $(PHP) php -d memory_limit=4G /usr/bin/composer
YARN                = $(NODE) yarn

##
## Setup
## -----
.PHONY: build kill install reset start stop clean no-docker assets watch restart uninstall

build:
	@$(DOCKER_COMPOSE) pull

kill:
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

install: clean .env.local build start vendor ## Install and start the project

reset: uninstall install ## Stop and start a fresh install of the project

start: ## Starts the docker containers
	$(DOCKER_COMPOSE) up -d --remove-orphans nginx
	bin/show-url.sh

stop: ## Stops the docker containers
	$(DOCKER_COMPOSE) stop

clean: kill ## Stop the project and remove generated files
	rm -rf vendor node_modules

no-docker:
	$(eval DOCKER_COMPOSE := \#)
	$(eval PHP := )
	$(eval NODE := )

assets: node_modules ## Run Webpack Encore to compile assets
	$(YARN) build

watch: node_modules ## Run Webpack Encore in watch mode
	$(YARN) run watch

restart: stop start ## Restarts the docker containers

uninstall: stop clean ## Uninstalls the project

composer.lock: composer.json
	$(COMPOSER) update --no-interaction

vendor: composer.lock
	$(COMPOSER) install

yarn.lock: package.json
	if [ ! -d "node_modules" ]; then $(YARN) install; fi
	$(YARN) upgrade

node_modules: yarn.lock
	$(YARN) install

##
## Tools
## -----

.PHONY: composer-install config-graph php-tools node-tools yarn yarn-build php-cs-fixer apply-php-cs-fixer show-url

composer-install: ## Runs composer install
	$(COMPOSER) install

php-tools: ## Starts the php-tools container
	$(PHP)

node-tools: ## Starts the node-tools container
	$(NODE)

yarn: ## Runs yarn
	$(YARN)

yarn-build: node_modules ## Runs yarn build
	$(YARN) build