/** Webpack configs for fifty sites */

const path = require('path');
const Encore = require('@symfony/webpack-encore');

/** PLUGINS */
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const autoprefixer = require('autoprefixer');

/** UTILS */
const getScssEntries = require('./webpack/helpersGetScssEntries');
const buildComponentStylePublicPath = require('./webpack/helpersBuildComponentStylePublicPath');

/** GLOBALS */
let GLOBALS = {
  outputPath: 'public',
  publicPath: 'build',
  applicationSrcDir: 'templates',
  fiftySrcDir: 'vendor/lefigaro/fifty/src/Resources/',
}

/** Set default configuration for Webpack Encore */
function setEncoreConfig() {
  // get all sass entries
  const { scssEntries } = getScssEntries(GLOBALS)
  Encore
    // be sure we start clean
    .reset();
  Encore
    // directory where compiled assets will be stored
    .setOutputPath(path.resolve(GLOBALS.outputPath, GLOBALS.application, 'fifty', 'build'))

    // public path used by the web server to access the output path
    .setPublicPath(path.resolve('/', GLOBALS.application, 'fifty', GLOBALS.publicPath))

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    // clean output directory before build
    .cleanupOutputBeforeBuild()

    // enable/disable sourcemaps
    .enableSourceMaps(GLOBALS.env !== 'production')

    // enable/disable versionning
    .enableVersioning(GLOBALS.env === 'production')

    // enable specific loaders (sass, js etc...)
    // loader that will handle components sass files
    .addLoader({
      test: /\.scss$/,
      exclude: [/node_modules/, /sass/],
      use: [
        {
          loader: 'cache-loader',
          options: {
            cacheDirectory: 'node_modules/.cache/cache-loader/',
          },
        },
        {
          loader: 'file-loader',
          options: {
            // Beware of generated component structure AND generated manifest file.
            // It must match with php AssetCollector logic
            // In path: component or components
            name: file => `css${buildComponentStylePublicPath(file)}[name].css`,
          },
        },
        'extract-loader',
        {
          loader: 'css-loader',
          options: {
            sourceMap: GLOBALS.env !== 'production',
            importLoaders: 2,
          },
        },
        {
          loader: 'postcss-loader',
          options: {
            sourceMap: GLOBALS.env === 'production' ? false : 'cheap-module-eval-source-map',
            plugins: [autoprefixer()]
          },
        },
        {
          loader: 'sass-loader',
          options: {
            sassOptions: {
              includePaths: [
                path.resolve(GLOBALS.applicationSrcDir, GLOBALS.application, 'views'),
                path.resolve(GLOBALS.applicationSrcDir, GLOBALS.application, 'views', '**/*.scss'),
                path.resolve(GLOBALS.fiftySrcDir, 'views'),
                path.resolve(GLOBALS.fiftySrcDir, 'views', '**/*.scss'),
                path.resolve('node_modules'),
              ],
            },
            sourceMap: GLOBALS.env !== 'production',
          },
        },
      ],
    })

    // loader that will handle global page sass files
    .addLoader({
      test: /\.scss$/,
      exclude: [/node_modules/, /views/],
      use: [
        MiniCssExtractPlugin.loader,
        {
          loader: 'css-loader',
          options: {
            sourceMap: GLOBALS.env !== 'production',
            importLoaders: 2,
          },
        },
        {
          loader: 'sass-loader',
          options: {
            sassOptions: {
              includePaths: [
                path.resolve(GLOBALS.applicationSrcDir, GLOBALS.application, 'sass'),
                path.resolve(GLOBALS.fiftySrcDir, 'sass'),
                path.resolve('node_modules'),
              ],
            },
            sourceMap: GLOBALS.env !== 'production',
          },
        },
      ],
    })

    // rewrite path for global page styles
    .addPlugin(
      new MiniCssExtractPlugin({
        filename: GLOBALS.env === 'production'
          ? 'css/globals/[name].[contenthash:4].css'
          : 'css/globals/[name].css',
      })
    )

    // configure manifest
    .configureManifestPlugin(options => {
      const customOptions = {
        fileName: 'assets.manifest.json',
        publicPath: `/${GLOBALS.application}/fifty/${GLOBALS.publicPath}/`,
        basePath: '',
        // Issue with minicssextractplugin: https://stackoverflow.com/questions/51596775/missing-entries-in-manifest-json
        writeToFileEmit: true,
        seed: {},
      };
      const mergedOptions = Object.assign(options, customOptions);
      return mergedOptions;
    })

    // configure fileNames and paths
    .configureFilenames({
      js: GLOBALS.env === 'production'
        ? 'js/[name].[contenthash:4].js'
        : 'js/[name].js',
      images: GLOBALS.env === 'production'
        ? 'images/[name].[hash:4].[ext]'
        : 'images/[name].[ext]',
      fonts: GLOBALS.env === 'production'
        ? 'fonts/[name].[hash:4].[ext]'
        : 'fonts/[name].[ext]',
    });

  // pass all sass entries to Encore
  if (Object.keys(scssEntries)) {
    [].forEach.call(Object.entries(scssEntries), ([entryName, entry]) => {
      if (/scss/g.test(entry)) {
        Encore.addStyleEntry(entryName, entry);
      }
    })
  }
}

const fifty = {
  getWebpackConfig: envConfiguration => {
    // we assume environment globals have already been defined and checked with convict
    // if no environment configuration found we throw immediately
    if (!envConfiguration) {
      throw new Error("Fifty webpack - No environment configuration object found.");
    }

    // get application globals
    const { env, PUBLIC_PATH: publicPath, CURRENT_APPLICATION: application } = envConfiguration;

    // merge all globals
    GLOBALS = { ...{ env, publicPath, application }, ...GLOBALS };

    // set common conf
    setEncoreConfig();
    const commonConf = Encore.getWebpackConfig();
    commonConf.name = 'common'

    // get additionnal environment conf
    let additionalConf = {};
    try {
      const requiredAdditionalConfig = require(`./webpack/config/${env}`);
      additionalConf = requiredAdditionalConfig.getWebpackConfig(Encore);
    } catch (error) {
      throw new Error("Fifty webpack - An error occured when getting additional configuration for this environment: ", env);
    }

    // merge both confs
    const finalConf = { ...commonConf, ...additionalConf };

    // last modifications
    finalConf.output.jsonpFunction = 'fiftyWebpackJsonp';

    // return one configuration object for webpack build
    return finalConf;
  }
};
module.exports = fifty;
